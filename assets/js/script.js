// Example expected result
// 0 is even
// 1 is odd
// two is not a number

let num = 0;

function evenOrodd(num) {
	if (typeof num === 'number') {
		if ( num%2 == 0 ) {
			console.log(`${num} is even.`)
		} else { 			//if ( num%2 >= 0 ) {		
			console.log(`${num} is odd.`)
		}
	}
	else {
		console.log(`${num} is not a number.`)
	}
}

evenOrodd("one");
evenOrodd(false);
evenOrodd(true);
evenOrodd(null);
evenOrodd(undefined);
evenOrodd(10);
evenOrodd(9);
evenOrodd(8);
evenOrodd(7);
evenOrodd(1);
evenOrodd(0);


	// if ( num%2 == 0 ) {
	// 	console.log(`${num} is even.`)
	// 	result = `${num} is even.`;
	// } else if ( num%2 >= 0 ) {
	// 	console.log(`${num} is odd.`)

	// } else {  			//if (typeof num.toString()) {
	// 	console.log(`${num} is not a number.`)
	// }